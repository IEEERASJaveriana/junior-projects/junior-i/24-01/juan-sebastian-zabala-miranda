import rclpy
from rclpy.node import Node
from turtlesim.srv import Clear, Spawn, SetPen, TeleportRelative

class ServiceClientNode(Node):
    def __init__(self):
        super().__init__('service_client_node')

        # Create client instances for each service
        self.clear_client = self.create_client(Clear, 'clear')
        self.spawn_client = self.create_client(Spawn, 'spawn')
        self.set_pen_client = self.create_client(SetPen, 'turtle1/set_pen')
        self.teleport_relative_client = self.create_client(TeleportRelative, 'turtle1/teleport_relative')

        # Call services sequentially
        self.call_clear()
        self.call_spawn()
        self.call_set_pen()
        self.call_teleport_relative()
        self.call_spawn_mrs_turtle2()
        self.call_set_pen_off()
        self.call_teleport_relative_angular()

    def call_clear(self):
        while not self.clear_client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('Servicio "clear" no disponible, esperando...')
        request = Clear.Request()
        future = self.clear_client.call_async(request)
        rclpy.spin_until_future_complete(self, future)

    def call_spawn(self):
        while not self.spawn_client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('Servicio "spawn" no disponible, esperando...')
        request = Spawn.Request()
        request.x = 5
        request.y = 0
        request.theta = 1
        future = self.spawn_client.call_async(request)
        rclpy.spin_until_future_complete(self, future)

    def call_set_pen(self):
        while not self.set_pen_client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('Servicio "set_pen" no disponible, esperando...')
        request = SetPen.Request()
        request.r = 255
        request.g = 0
        request.b = 0
        request.width = 5
        request.off = False
        future = self.set_pen_client.call_async(request)
        rclpy.spin_until_future_complete(self, future)

    def call_teleport_relative(self):
        while not self.teleport_relative_client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('Servicio "teleport_relative" no disponible, esperando...')
        request = TeleportRelative.Request()
        request.linear = 0
        request.angular = 0
        future = self.teleport_relative_client.call_async(request)
        rclpy.spin_until_future_complete(self, future)

    def call_spawn_mrs_turtle2(self):
        while not self.spawn_client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('Servicio "spawn" no disponible, esperando...')
        request = Spawn.Request()
        request.name = 'mrsTurtle2'
        request.x = 2
        request.y = 2
        request.theta = 0
        future = self.spawn_client.call_async(request)
        rclpy.spin_until_future_complete(self, future)

    def call_set_pen_off(self):
        while not self.set_pen_client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('Servicio "set_pen" no disponible, esperando...')
        request = SetPen.Request()
        request.off = True
        future = self.set_pen_client.call_async(request)
        rclpy.spin_until_future_complete(self, future)

    def call_teleport_relative_angular(self):
        while not self.teleport_relative_client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('Servicio "teleport_relative" no disponible, esperando...')
        request = TeleportRelative.Request()
        request.linear = 0
        request.angular = 2
        future = self.teleport_relative_client.call_async(request)
        rclpy.spin_until_future_complete(self, future)

def main(args=None):
    rclpy.init(args=args)
    service_client_node = ServiceClientNode()
    rclpy.spin(service_client_node)
    service_client_node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()