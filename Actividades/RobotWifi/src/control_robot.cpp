#include <Arduino.h>
#include <ESP8266WiFi.h>

const int LED = 2;
const int ENA = 3;
const int IN1 = 0;
const int IN2 = 5;
const int IN3 = 4;
const int IN4 = 14;
const int ENB = 12;
const int ECHO = 13;
const int TRIGGER = 15;

int port = 8144;
WiFiServer server(port);

const char *ssid = "dlc";
const char *passwd = "eldenring";

double t = 0.0;
double d = 0.0;

int vel = 5;

void movimiento(int x, int dis);
void adelante();
void atras();
void izquierda();
void derecha();
void cambiar_vel(int x);
double read_sensor(); 
void apagar_robot();


void setup() {
  Serial.begin(115200);
  pinMode(LED, OUTPUT);
  digitalWrite(LED, HIGH);


  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, passwd);
  while(WiFi.status() != WL_CONNECTED){
    Serial.print(".");
  }
  Serial.println(" ");

 
  Serial.println(WiFi.localIP());
  digitalWrite(LED, LOW);
  server.begin();


  pinMode(ENA, OUTPUT); 
  pinMode(IN1, OUTPUT); 
  pinMode(IN2, OUTPUT); 
  pinMode(IN3, OUTPUT); 
  pinMode(IN4, OUTPUT); 
  pinMode(ENB, OUTPUT); 
  pinMode(TRIGGER, OUTPUT); 
  pinMode(ECHO, INPUT); 
}

void loop() {
  delay(10);
  WiFiClient client = server.accept();
  String read_ros = "";
  read_sensor();

  if(client){
      if(client.connected()){
        Serial.print("Cliente conectado");
      }

      while(client.connected()){
        digitalWrite(LED, HIGH);
        d = read_sensor();
        while(client.available() > 0) {
          char data = client.read();
          read_ros.concat(data);
        }

        if(read_ros.compareTo("") != 0) {
          Serial.println(read_ros);
          int x = read_ros.toInt();
        
          /*if(d < 25.0){
            x = 0;
          }*/

          if(x == 0){
            apagar_robot();
          } else{
            movimiento(x, d);
          }
            
          read_ros = "";
        }
      }
      client.stop();
      Serial.println("Cliente desconectado");
      apagar_robot();
  }
  apagar_robot();
}

void movimiento(int x, int dis){
  if(x == 1){
    if(dis < 25.0){
      apagar_robot();
    }else{
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, HIGH);
    }
  }else if(x == 2){
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, HIGH);
    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, LOW);
  }else if(x == 3){
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);
    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, LOW);
  }else if(x == 4){
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, HIGH);
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, HIGH); 
  }else if(x == 5){
    vel+= 5;
    //Serial.println("Nueva velocidad: " + vel);
    //Serial.println(vel);
  }else if(x == 6){
    vel -= 5;
    //Serial.println("Nueva velocidad: ");
    //Serial.println(vel);
  }
  digitalWrite(ENA, vel);
  digitalWrite(ENB, vel);
}

double read_sensor() {
  digitalWrite(TRIGGER, HIGH);
  delayMicroseconds(10);          
  digitalWrite(TRIGGER, LOW);
  t = pulseIn(ECHO, HIGH, 30000); 
  if (t == 0) {
    d = 1000.0; 
  } else {
    d = t / 59.0;
  }
  //Serial.print("Distancia: ");
  //Serial.println(d);
  return d;
}

void apagar_robot() {
  digitalWrite(ENA, 0);
  digitalWrite(ENB, 0);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, LOW);
}