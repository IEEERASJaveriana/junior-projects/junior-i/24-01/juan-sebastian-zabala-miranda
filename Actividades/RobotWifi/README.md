# Projecto Arduino 

Nombre del proyecto: Terraneitor 
Integrantes:
José Elkin Estrada Morales 
Juan Sebastián Zabala Miranda
Función del código y del nodo:
El código se divide en dos partes:
La primera siendo la que establece la conexión wifi entre en el computador y el Arduino, las teclas utilizadas para el control del movimiento y velocidad, también maneja a que distancia el robot se detiene teniendo en cuanta el sensor de proximidad. 

La segunda parte usa un nido de turtlesim para poder manejar el robot mediante teclas.

Funciones de ros utilizadas:
socket_transmiter_a :Se utiliza para establecer la conexión con el Arduino
Tutle_telo_key:Se usa para manejar el robot mediante teclas