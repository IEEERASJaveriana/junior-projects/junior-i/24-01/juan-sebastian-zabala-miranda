import rclpy
from rclpy.node import Node
from turtlesim.srv import SetPen

class ChangeTurtleColor(Node):
    def __init__(self):
        super().__init__('change_turtle_color')
        self.client = self.create_client(SetPen, 'turtle1/set_pen')

    def change_turtle_path_color(self):
        while not self.client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('Servicio no disponible, esperando...')
        request = SetPen.Request()
        request.r = 0  # Componente roja (0-255)
        request.g = 255  # Componente verde (0-255)
        request.b = 0  # Componente azul (0-255)
        request.width = 2  # Ancho del trazo
        request.off = 0  # No borrar la trayectoria existente
        future = self.client.call_async(request)
        rclpy.spin_until_future_complete(self, future)
        if future.result() is not None:
            self.get_logger().info('Color de la trayectoria cambiado a verde')
        else:
            self.get_logger().error('Error al cambiar el color de la trayectoria')

def main(args=None):
    rclpy.init(args=args)
    change_turtle_color = ChangeTurtleColor()
    change_turtle_color.change_turtle_path_color()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
