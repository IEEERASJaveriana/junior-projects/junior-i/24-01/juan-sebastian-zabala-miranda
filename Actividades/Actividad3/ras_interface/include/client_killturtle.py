import rclpy
from rclpy.node import Node
from turtlesim.srv import Kill

class KillTurtle(Node):
    def __init__(self):
        super().__init__('kill_turtle')
        self.client = self.create_client(Kill, 'kill')

    def kill_turtle_1(self):
        while not self.client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('Servicio no disponible, esperando...')
        request = Kill.Request()
        request.name = 'turtle1'  # Nombre de la tortuga a matar
        future = self.client.call_async(request)
        rclpy.spin_until_future_complete(self, future)
        if future.result() is not None:
            self.get_logger().info('Tortuga turtle_1 eliminada')
        else:
            self.get_logger().error('Error al eliminar la tortuga turtle_1')

def main(args=None):
    rclpy.init(args=args)
    kill_turtle = KillTurtle()
    kill_turtle.kill_turtle_1()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
