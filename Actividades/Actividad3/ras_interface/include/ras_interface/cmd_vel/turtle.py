import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Twist

class MoveTurtle(Node):
    def __init__(self):
        super().__init__('move_turtle')
        self.publisher_ = self.create_publisher(Twist, 'turtle1/cmd_vel', 10)
        timer_period = 0.5  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)

    def timer_callback(self):
        msg = Twist()
        msg.linear.x = 1.0  # Velocidad lineal en el eje x
        msg.angular.z = 0.0  # Velocidad angular (en este caso, no giramos)
        self.publisher_.publish(msg)
        self.get_logger().info('Moving turtle in the x-axis')

def main(args=None):
    rclpy.init(args=args)
    move_turtle = MoveTurtle()
    rclpy.spin(move_turtle)
    move_turtle.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
