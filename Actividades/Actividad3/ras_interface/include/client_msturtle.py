import rclpy
from rclpy.node import Node
from turtlesim.srv import Spawn

class CreateTurtle(Node):
    def __init__(self):
        super().__init__('create_turtle')
        self.client = self.create_client(Spawn, 'spawn')

    def create_new_turtle(self):
        while not self.client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('Servicio no disponible, esperando...')
        request = Spawn.Request()
        request.x = 5.0  # Posición x de la nueva tortuga
        request.y = 5.0  # Posición y de la nueva tortuga
        request.theta = 0.0  # Ángulo inicial de la nueva tortuga
        request.name = 'mrs_turtle'  # Nombre de la nueva tortuga
        future = self.client.call_async(request)
        rclpy.spin_until_future_complete(self, future)
        if future.result() is not None:
            self.get_logger().info('Nueva tortuga creada: mrs_turtle')
        else:
            self.get_logger().error('Error al crear la nueva tortuga')

def main(args=None):
    rclpy.init(args=args)
    create_turtle = CreateTurtle()
    create_turtle.create_new_turtle()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
