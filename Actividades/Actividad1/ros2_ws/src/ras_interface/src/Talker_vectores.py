#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from ras_interface.msg import Vectores3 # type: ignore
import random

class Vectores3Publisher(Node):
    def __init__(self):
        super().__init__('vectores3_publisher')
        self.publisher_ = self.create_publisher(Vectores3, 'vectores', 10)
        timer_period = 1.0  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)

    def timer_callback(self):
        msg = Vectores3()
        msg.x = random.uniform(0.0, 100.0)
        msg.y = random.uniform(0.0, 100.0)
        msg.z = random.uniform(0.0, 100.0)
        self.publisher_.publish(msg)
        self.get_logger().info('Publishing: x=%f, y=%f, z=%f' % (msg.x, msg.y, msg.z))

def main(args=None):
    rclpy.init(args=args)
    vectores3_publisher = Vectores3Publisher()
    rclpy.spin(vectores3_publisher)
    vectores3_publisher.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
