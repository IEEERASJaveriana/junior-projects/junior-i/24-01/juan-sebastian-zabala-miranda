#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from ras_interface.msg import Vectores3 # type: ignore

class Vectores3Subscriber(Node):
    def __init__(self):
        super().__init__('vectores3_subscriber')
        self.subscription = self.create_subscription(
            Vectores3,
            'vectores',
            self.listener_callback,
            10)
        self.subscription  # prevent unused variable warning

    def listener_callback(self, msg):
        self.get_logger().info('Received: x=%f, y=%f, z=%f' % (msg.x, msg.y, msg.z))

def main(args=None):
    rclpy.init(args=args)
    vectores3_subscriber = Vectores3Subscriber()
    rclpy.spin(vectores3_subscriber)
    vectores3_subscriber.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
