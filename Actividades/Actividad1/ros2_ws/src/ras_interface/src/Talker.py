#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from std_msgs.msg import Float64
import random

class Float64Publisher(Node):
    def __init__(self):
        super().__init__('float64_publisher')
        self.publisher_ = self.create_publisher(Float64, 'float_message', 10)
        timer_period = 1.0  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)

    def timer_callback(self):
        msg = Float64()
        msg.data = random.uniform(0.0, 100.0)
        self.publisher_.publish(msg)
        self.get_logger().info('Publishing: "%f"' % msg.data)

def main(args=None):
    rclpy.init(args=args)
    float64_publisher = Float64Publisher()
    rclpy.spin(float64_publisher)
    float64_publisher.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
