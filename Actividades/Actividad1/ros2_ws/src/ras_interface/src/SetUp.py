from setuptools import setup

package_name = 'ras_interface'

setup(
    name=package_name,
    version='0.0.0',
    packages=[],
    py_modules=[
        'src.talker',
        'src.listener',
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='juan',
    maintainer_email='ossezabalamiranda@gmail.com',
    description='ROS 2 package for ras_interface',
    license='Apache License 2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'talker = src.talker:main',
            'listener = src.listener:main',
        ],
    },
)
