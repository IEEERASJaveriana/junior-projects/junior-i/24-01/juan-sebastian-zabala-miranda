#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Twist
import random

class TwistPublisher(Node):
    def __init__(self):
        super().__init__('twist_publisher')
        self.publisher_ = self.create_publisher(Twist, 'twist_call', 10)
        timer_period = 1.0  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)

    def timer_callback(self):
        msg = Twist()
        msg.linear.x = random.uniform(-1.0, 1.0)
        msg.linear.y = random.uniform(-1.0, 1.0)
        msg.linear.z = random.uniform(-1.0, 1.0)
        msg.angular.x = random.uniform(-1.0, 1.0)
        msg.angular.y = random.uniform(-1.0, 1.0)
        msg.angular.z = random.uniform(-1.0, 1.0)
        self.publisher_.publish(msg)
        self.get_logger().info('Publishing: Linear=%s, Angular=%s' % (str(msg.linear), str(msg.angular)))

def main(args=None):
    rclpy.init(args=args)
    twist_publisher = TwistPublisher()
    rclpy.spin(twist_publisher)
    twist_publisher.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
