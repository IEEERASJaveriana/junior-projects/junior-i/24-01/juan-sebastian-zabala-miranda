#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from std_msgs.msg import Float64

class Float64Subscriber(Node):
    def __init__(self):
        super().__init__('float64_subscriber')
        self.subscription = self.create_subscription(
            Float64,
            'float_message',
            self.listener_callback,
            10)
        self.subscription  # prevent unused variable warning

    def listener_callback(self, msg):
        self.get_logger().info('Received: "%f"' % msg.data)

def main(args=None):
    rclpy.init(args=args)
    float64_subscriber = Float64Subscriber()
    rclpy.spin(float64_subscriber)
    float64_subscriber.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
