#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from ras_interface.srv import Vectores3 # type: ignore

class EuclideanDistanceClient(Node):
    def __init__(self):
        super().__init__('euclidean_distance_client')

    def request_euclidean_distance(self, x, y, z):
        client = self.create_client(Vectores3, 'euclidean_distance')
        while not client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('Service not available, waiting again...')
        request = Vectores3.Request()
        request.x = x
        request.y = y
        request.z = z
        future = client.call_async(request)
        while rclpy.ok():
            rclpy.spin_once(self)
            if future.done():
                try:
                    response = future.result()
                except Exception as e:
                    self.get_logger().error(f'Service call failed: {e}')
                else:
                    self.get_logger().info('Euclidean distance: %f' % response.euclidiano)
                break

def main(args=None):
    rclpy.init(args=args)
    euclidean_distance_client = EuclideanDistanceClient()
    # Here you can get x, y, z from user input or pass constants as required
    x = float(input('Enter x: '))
    y = float(input('Enter y: '))
    z = float(input('Enter z: '))
    euclidean_distance_client.request_euclidean_distance(x, y, z)
    rclpy.shutdown()

if __name__ == '__main__':
    main()
