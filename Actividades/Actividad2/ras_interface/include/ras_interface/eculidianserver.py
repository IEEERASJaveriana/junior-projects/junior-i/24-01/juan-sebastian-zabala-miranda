#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from ras_interface.srv import Vectores3 # type: ignore

class EuclideanDistanceServer(Node):
    def __init__(self):
        super().__init__('euclidean_distance_server')
        self.srv = self.create_service(Vectores3, 'euclidean_distance', self.euclidean_distance_callback)
        self.get_logger().info('Server is ready to calculate euclidean distance...')

    def euclidean_distance_callback(self, request, response):
        # Calculate the Euclidean distance
        euclidean_distance = (request.x**2 + request.y**2 + request.z**2)**0.5
        response.euclidiano = euclidean_distance
        self.get_logger().info('Received vectors: x=%f, y=%f, z=%f' % (request.x, request.y, request.z))
        self.get_logger().info('Euclidean distance: %f' % euclidean_distance)
        return response

def main(args=None):
    rclpy.init(args=args)
    euclidean_distance_server = EuclideanDistanceServer()
    rclpy.spin(euclidean_distance_server)
    euclidean_distance_server.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
