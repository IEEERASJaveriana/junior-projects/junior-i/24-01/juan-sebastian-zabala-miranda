from setuptools import setup

package_name = 'ras_interface'

setup(
    name=package_name,
    version='0.0.0',
    packages=[],
    py_modules=[
        'src.talker',
        'src.listener',
        'src.talker_vectores',
        'src.listener_vectores',
        'src.twist_publisher',
        'src.twist_subscriber',
        'src.euclidean_distance_server',
        'src.euclidean_distance_client',
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='juan',
    maintainer_email='ossezabalamiranda@gmail.com',
    description='ROS 2 package for ras_interface',
    license='Apache License 2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'euclidean_distance_server = src.euclidean_distance_server:main',
            'euclidean_distance_client = src.euclidean_distance_client:main',
        ],
    },
)
